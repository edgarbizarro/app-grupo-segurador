import React from 'react';
import { Platform } from 'react-native'


const params = {
    icon_bell: {
        name:'bell',
        color: Platform.OS == "ios" ? 'blue' : 'white',
        size:22
    },
    icon_envelope:{
        name: 'envelope',
        color: Platform.OS == "ios" ? '#5067FF' : 'white',
        size: 22
    },
    icon_plus: {
        name:'plus',
        color:'white',
        size:18
    },
    icon_file:{
        name:'eye',
        color:'white',
        size:22
    },
    icon_rename:{
        name:'pencil-alt',
        color:'white',
        size:22
    },
    icon_remove:{
        name:'times',
        color:'white',
        size:22
    },
    icon_whatsapp:{
        name: "logo-whatsapp"
    },
    icon_facebook:{
        name: "logo-facebook"
    },
    icon_mail:{
        name: "mail"
    },
    spinner:{
        color:'blue'
    },

    datepicker:{
        defaultDt: new Date(),
        minimumDt: new Date(1930, 12, 31),
        maximumDt: new Date(),
        locale: "pt-BR",
        timeZone: undefined,
        modalTransparent: Platform.OS == "ios" ? true : false,
        animationType: "fade",
        androidMode: "default",
        placeHolderText:"Selecione",
        textStyle: {color: "black"},
        placeHolderTextStyle: {color: "#d3d3d3", paddingBottom: 40},
        disabled: false
    },

    SectionedMultiSelect:{
        searchPlaceholderText: "Pesquisar",
        selectText: "Selecione...",
        confirmText: "Confirmar",
        showDropDowns: true,
        readOnlyHeadings: true,
    },



}

export default params
