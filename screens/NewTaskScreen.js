import React, { Component } from 'react';
import {Platform, StyleSheet, View, Image, Alert, Modal, TouchableHighlight} from 'react-native';
import { Container, Content, Header,
         List, ListItem, Thumbnail,
         Left, Body, Right,
         Button, Icon, Fab,
         Title, Text, Badge,
         Footer, FooterTab, Form,
         Input, Item, Textarea,
         Label, Spinner, Picker,
         DatePicker, Row } from 'native-base';
import {LogoTitle } from '../components/LogoTitle';
import SyncStorage from 'sync-storage';
import Service from '../services/Service';
import { create } from 'apisauce';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import { taskId } from '../src/store/actions/task';
import { TextInputMask } from 'react-native-masked-text'
import style from '../src/style';
import params from '../services/params';


class NewTaskScreen extends Component {

  static navigationOptions = {
      title: 'Novo Afiliado',
      header: null
  }

  constructor(props) {
    super(props);

    this.state = {
      chosenDate: new Date(),
      name: '',
      email: '',
      phone: '',
      company: '',
      city: '',
      state: '',
      observation: '',
      loadPage: 0,
      active: false,
      loading: '',
      sending: 0,
    };

    this.setDate = this.setDate.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePhone = this.onChangePhone.bind(this);
    this.onChangeCompany = this.onChangeCompany.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.onChangeState = this.onChangeState.bind(this);
    this.onChangeObservation = this.onChangeObservation.bind(this);
  }

  componentDidMount(){
    storage = {
      token: this.props.user.token
    }
  }

  setDate(newDate) {
    console.log('buttonsVisible',this.state.buttonsVisible)
    this.setState({ buttonsVisible: !this.state.buttonsVisible})
    this.setState({ chosenDate: newDate.toLocaleDateString("pt-BR") });
  }

  onChangeName(name) {
    this.setState({
      name
    })
  }

  onChangeEmail(email) {
    this.setState({
      email
    })
  }

  onChangePhone(phone) {
    this.setState({
      phone
    })
  }

  onChangeCompany(company) {
    this.setState({
      company
    })
  }

  onChangeCity(city) {
    this.setState({
      city
    })
  }

  onChangeState(state) {
    this.setState({
      state
    })
  }

  onChangeObservation(observation) {
    this.setState({
      observation
    })
  }

  drawerToggle(){
    this.props.navigation.toggleDrawer();
  }

  createTask(){
    // if(typeof(this.state.chosenDate) == 'object'){
    //   return Alert.alert('Erro no formulário', 'Preencha o campo Data Nascimento!',[
    //           {
    //             text: 'Ok',
    //             onPress: () => console.log('Cancel Pressed'),
    //             style: 'cancel',
    //           },
    //         ])
    // }

    if(this.state.name == ''){
      return Alert.alert('Erro no formulário', 'Preencha o campo nome!',[
              {
                text: 'Ok',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ])
    }

    if(this.state.email == ''){
      return Alert.alert('Erro no formulário', 'Preencha o campo email!',[
              {
                text: 'Ok',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ])
    }

    if(this.state.email == ''){
      return Alert.alert('Erro no formulário', 'Preencha o campo email!',[
        {
          text: 'Ok',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ])
    }


    if(this.state.email != ''){
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      let isValid = reg.test(this.state.email) == 0;
      if(isValid){
        return Alert.alert('Erro no formulário', 'Campo email esta incorreto!',[
          {
            text: 'Ok',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ])
      }
    }

    store = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      birthday: this.state.chosenDate,
      company: this.state.company,
      city: this.state.city,
      state: this.state.state,
      observation: this.state.observation,
      token: this.props.user.token,
    }

    console.log('teste Store', store);

    this.setState({
      sending: 1
    })

    Service.createtask(store).then( success => {
      console.log('teste de sucesso', success);
      Alert.alert('Afiliado salvo', 'Afiliado criado com sucesso',[
        {
          text: 'OK',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ])
      this.setState({
        sending: 0,
        name: '',
        email: '',
        phone: '',
        company: '',
        city: '',
        state: '',
        observation: '',
      })
      // console.log('retorno Create,,,,,,,', success.data)
      this.props.navigation.navigate('MyLists');
    }).catch(error => {
      console.log('teste de erro', success);
      this.setState({
        sending: 0,
        name: '',
        email: '',
        phone: '',
        company: '',
        city: '',
        state: '',
        observation: '',
      })
      Alert.alert('Não foi possivel criar o afiliado', 'Tente novamente ou tente mais tarde!',[
        {
          text: 'Cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ])
      console.log('erro', error)
    })

  }

  componentDidUpdate(){
    if(this.state.loadPage == 0){
      this.setState({
        loadPage: 1
      })
    }
  }

  render() {

    if(this.state.sending == 1){
      return (
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Icon name='menu' onPress={ () => this.drawerToggle() }/>
              </Button>
            </Left>
            <Body>
              <Text style={ style.styleTitle }>Novo Afiliado</Text>
            </Body>

          </Header>
          <Content padder >
            <Spinner color={params.spinner.color} />
          </Content>
        </Container>
      )
    }


      return (
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Icon name='menu' onPress={ () => this.drawerToggle() }/>
              </Button>
            </Left>
            <Body>
              <Text style={ style.styleTitle }>Novo Afiliado</Text>
            </Body>
            <Right>

            </Right>
          </Header>
          <Content padder >
            <Form style={style.NewTasksScreen_Form}>

              <Label>Nome*</Label>
              <Input value={ this.state.name } onChangeText={ this.onChangeName } placeholder="Nome Completo" placeholderTextColor='#d3d3d3'style={{fontSize: 16, paddingLeft:10}} />

              <Label>Email*</Label>
              <Input value={ this.state.email } onChangeText={ this.onChangeEmail } placeholder="Email@email.com" placeholderTextColor='#d3d3d3'style={{fontSize: 16, paddingLeft:10}} />

              <Label>Telefone</Label>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(99) '
                  }}
                  value={this.state.phone}
                  onChangeText={this.onChangePhone}
                  placeholder="(xx) xxxx-xxxx"
                />


              <Label>Data Nascimento</Label>
              <DatePicker
                defaultDate={params.datepicker.defaultDt}
                minimumDate={params.datepicker.minimumDt}
                maximumDate={params.datepicker.maximumDt}
                locale={params.datepicker.locale}
                timeZoneOffsetInMinutes={params.datepicker.timeZone}
                modalTransparent={params.datepicker.modalTransparent}
                animationType={params.datepicker.animationType}
                androidMode={params.datepicker.androidMode}
                placeHolderText={params.datepicker.placeHolderText}
                textStyle={params.datepicker.textStyle}
                placeHolderTextStyle={params.datepicker.placeHolderTextStyle}
                onDateChange={this.setDate}
                disabled={params.datepicker.disabled}
              />

              <Label>Empresa</Label>
                <Input value={ this.state.company } onChangeText={ this.onChangeCompany } placeholder="Nome completo" placeholderTextColor='#d3d3d3'style={{fontSize: 16, paddingLeft:10}} />

              <Label>Cidade</Label>
              <Input value={ this.state.city } onChangeText={ this.onChangeCity } placeholder="Aa..." placeholderTextColor='#d3d3d3'style={{fontSize: 16, paddingLeft:10}} />

              <Label>Estado</Label>
                <Picker
                mode="dropdown"
                iosHeader="Select your SIM"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                selectedValue={this.state.state}
                onValueChange={this.onChangeState.bind(this)}
              >
                <Picker.Item label="São Paulo" value="SP" />
                <Picker.Item label="Acre" value="AC" />
                <Picker.Item label="Alagoas" value="AL" />
                <Picker.Item label="Amapá" value="AP" />
                <Picker.Item label="Amazonas" value="AM" />
                <Picker.Item label="Bahia" value="BA" />
                <Picker.Item label="Ceará" value="CE" />
                <Picker.Item label="Distrito Federal" value="DF" />
                <Picker.Item label="Espírito Santo" value="ES" />
                <Picker.Item label="Goiás" value="GO" />
                <Picker.Item label="Maranhão" value="MA" />
                <Picker.Item label="Mato Grosso" value="MT" />
                <Picker.Item label="Mato Grosso do Sul" value="MS" />
                <Picker.Item label="Minas Gerais" value="MG" />
                <Picker.Item label="Pará" value="PA" />
                <Picker.Item label="Paraíba" value="PB" />
                <Picker.Item label="Paraná" value="PR" />
                <Picker.Item label="Pernambuco" value="PE" />
                <Picker.Item label="Piauí" value="PI" />
                <Picker.Item label="Rio de Janeiro" value="RJ" />
                <Picker.Item label="Rio Grande do Norte" value="RN" />
                <Picker.Item label="Rio Grande do Sul" value="RS" />
                <Picker.Item label="Rondônia" value="RO" />
                <Picker.Item label="Roraima" value="RR" />
                <Picker.Item label="Santa Catarina" value="SC" />
                <Picker.Item label="Sergipe" value="SE" />
                <Picker.Item label="Tocantins" value="TO" />
              </Picker>


              <Label>Observação</Label>
              <Textarea rowSpan={5} bordered placeholder="Aa..." value={ this.state.observation } onChangeText={ this.onChangeObservation } placeholderTextColor='#d3d3d3' style={{fontSize: 16, paddingLeft:10}} />



            </Form>

          </Content>


          <Fab
            active="true"
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.createTask() }>
            <FontAwesome5 name={'check'} color={'white'}  size={ 18} />
          </Fab>
        </Container>
      );


  }
}

const mapStateToProps = ({ user,task }) => {
  return { user,task }
}

const mapDispatchToProps = {
  taskId
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTaskScreen)
