import { NOTIFICATIONS, CLEAR_NOTIFICATIONS } from './actionTypes'
import Service from '../../../services/Service';
  
export const clear_notifications = () => {
    return {
        type: CLEAR_NOTIFICATIONS
    }
}

export const get_notification = notifications => {
    return {
      type: NOTIFICATIONS,
      payload: notifications
    }
  }
  
export function notifications(notifications) {
    return dispatch => {
      Service.totalnotifications(notifications)
        .then( success => {
          if (success.ok) {
              dispatch({
                type: NOTIFICATIONS,
                payload: success.data
              });
          } else {
            console.log('Erro na requisição', success);
          }
        }).catch(error => {
          console.log('erro', error)
          dispatch({ type: CLEAR_NOTIFICATIONS });
        })
    }
}