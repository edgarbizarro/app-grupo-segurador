import { NOTIFICATIONLIST, REMOVE_NOTIFICATIONLIST, REFRESH } from '../actions/actionTypes'

const initialState = {
  chamados: [],
  refresh: false
}

const reducer = (state = initialState, action ) => {
  // console.log('Reducer LOCKEDLIST', action);
  switch (action.type) {
    case NOTIFICATIONLIST:
      console.log('Reducer NOTIFICATIONLIST', action);
      return {
        ...state,
        chamados: action.payload
      }
    case REMOVE_NOTIFICATIONLIST:
      return {
        ...state,
        chamados: []
      }
    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      }
    default:
      return state
  }
}

export default reducer
