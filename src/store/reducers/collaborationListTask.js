import { COLLABORATIONLIST, REMOVE_COLLABORATIONLIST, REFRESH } from '../actions/actionTypes'

const initialState = {
  chamados: [],
  refresh: false
}

const reducer = (state = initialState, action ) => {
  switch (action.type) {
    case COLLABORATIONLIST:
      return {
        ...state,
        chamados: action.payload
      }
    case REMOVE_COLLABORATIONLIST:
      return {
        ...state,
        chamados: []
      }
    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      }
    default:
      return state
  }
}

export default reducer
