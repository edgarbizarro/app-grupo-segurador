import React, { Component } from 'react';
import {Platform, StyleSheet, View, Image, Alert, AsyncStorage, TouchableOpacity, FlatList, RefreshControl} from 'react-native';
import { Container, Content, Header, List, ListItem, Thumbnail, Left, Body, Right, Button, Icon, Fab, Title, Text, Badge, Spinner } from 'native-base';
import {LogoTitle } from '../components/LogoTitle';
import SyncStorage from 'sync-storage';
import Service from '../services/Service';
import { create } from 'apisauce';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ActionButton from 'react-native-action-button';
import PTRView from 'react-native-pull-to-refresh';
import { connect } from 'react-redux';
import { task, viewtask } from '../src/store/actions/task';
import { listTask, teste1, mylists } from '../src/store/actions/listTask';
import storage from 'redux-persist/lib/storage';
import params from '../services/params'
import style from '../src/style'

class MyListsScreen extends Component {

  static navigationOptions = {
      title: 'Afiliados',
      header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      lists: [],
      loadPage: 0,
      empty: true
    };
  }

  drawerToggle(){
    this.props.navigation.toggleDrawer();
  }

  createTask(){
    this.props.navigation.navigate('NewTask');
  }

  _redirectViewTask(id) {

    this.props.viewtask({
      id: id,
      token: this.props.user.token
    });

    setTimeout( () => {
      this.props.mylists({
        token: this.props.user.token
      });
    },1500)

    this.setState({
      loadPage: 1
    });


    this.props.navigation.navigate('ViewTask');
  }

  isEmpty(obj){
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

  componentDidMount(){

    console.log('2 THIS.PROPS 2', this.props.lista_afiliado);

    this.props.mylists({
      token: this.props.user.token
    });

    if( this.isEmpty(this.props.lista_afiliado.afiliaetes.afiliaetes) ){
      this.setState({
        empty: true
      })
    }

  }

  _refresh(){
    console.log('pullRefresh');

      this.props.mylists({
        token: this.props.user.token
      });

  }

  _keyExtractor = (item, index) => item.id;

  _renderItem = ({item}) => (
    <ListItem thumbnail onPress={ () => this._redirectViewTask( item.id )}>
      <Left>
        <Thumbnail circle source={ require( "../assets/imgs/afiliaete_app.jpg") } />
      </Left>
      <Body>
        <Text numberOfLines={2} adjustsFontSizeToFit>{item.name}</Text>
        {(item.email != '') ?
          <Text note numberOfLines={1} adjustsFontSizeToFit>{item.email}</Text> : <Text note numberOfLines={1} adjustsFontSizeToFit>{item.phone}</Text>
        }
      </Body>
    </ListItem>
  );

  componentDidUpdate()
  {
    if(this.state.loadPage == 0){
      this.props.mylists({
        token: this.props.user.token
      });

      this.setState({
        loadPage: 1
      })
    }
  }

  render() {

    if(this.state.empty == true){
      rs = <Spinner color={params.spinner.color} style={style.MyTasksScreen_Spinner} />;
    }else{
      rs = <Text>Nenhuma Tarefa</Text>;
    }

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' onPress={ () => this.drawerToggle() }/>
            </Button>
          </Left>
          <Body>
            <Text style={ style.styleTitle }>Afiliados</Text>
          </Body>
          <Right>

          </Right>

        </Header>
              {( this.props.lista_afiliado.afiliaetes.length == 0) ?
                <Spinner color='blue' style={style.MyTasksScreen_Spinner} /> : <TouchableOpacity></TouchableOpacity>}
              <FlatList
                data={this.props.lista_afiliado.afiliaetes.afiliaetes}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
                refreshControl={
                  <RefreshControl
                    refreshing={this.props.lista_afiliado.refresh}
                    onRefresh={this._refresh.bind(this)}
                  />
                }
              />
        <Fab
          active="true"
          direction="up"
          containerStyle={{ }}
          style={ style.MyTasksScreen_Fab }
          position="bottomRight"
          onPress={() => this.createTask()}>
          <FontAwesome5 name={params.icon_plus.name} color={params.icon_plus.color} size={params.icon_plus.size} />
        </Fab>
      </Container>
    );
  }

}

const mapStateToProps = ({ user, task, listTask }) => {
  return { user,task, lista_afiliado: listTask }
}

const mapDispatchToProps = {
  task,
  listTask,
  viewtask,
  teste1,
  mylists,
}

export default connect(mapStateToProps, mapDispatchToProps)(MyListsScreen)
