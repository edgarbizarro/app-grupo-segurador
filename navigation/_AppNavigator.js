import React from 'react';
import {
    createAppContainer,
    createDrawerNavigator,
    createStackNavigator,
} from 'react-navigation';

import LoginScreen from '../screens/LoginScreen';
import MyTasksScreen from '../screens/MyTasksScreen';

const DrawerStack = createDrawerNavigator({
    MyTasks: MyTasksScreen,
    // ViewCard: ViewCardScreen,
});

const RootStack = createStackNavigator({
    Login: LoginScreen,
    MyTasks: DrawerStack
});

export default RootStack;
