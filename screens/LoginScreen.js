/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Platform, StyleSheet, View, Image, Alert } from 'react-native';
import { Container, Thumbnail, Content, Form, Item, Input, Label, Button, Text } from 'native-base';
import { create } from 'apisauce';
import { StackActions, NavigationActions } from 'react-navigation';
import Service from '../services/Service';
import SyncStorage from 'sync-storage';
import { connect } from 'react-redux';
import { login } from '../src/store/actions/user'

class LoginScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      pass: '',
      token: '',
      date: '',
      isAuthenticated: false,
      textButton: 'ENTRAR'
    };
    this.manipularUsuario = this.manipularUsuario.bind(this);
    this.manipularPass = this.manipularPass.bind(this);


    if(this.props.user.token){
      // Redireciona e reseta a pilha
      this.props
      .navigation
      .dispatch(StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'MyLists'})
        ]
       }));
    }
  }

  state = {
    email: '',
    password: '',
    token: '',
    nome: '',
  }

  manipularUsuario(user) {
    this.setState({
      user
    });
  }

  manipularPass(pass) {
    this.setState({
      pass
    });
  }

  _submitLogin(){

    const login = {
      email: this.state.user,
      password: this.state.pass,
    }

    console.log(login)

    this.setState({
      textButton: "CARREGANDO"
    })


    Service.login(login).then( success => {
      if(success.data.message == "Usuário ou senha inválidos"){
        Alert.alert("Usuário não encontrado","Verifique seu email e sua senha, tente novamente!")
        this.setState({
          textButton: "ENTRAR"
        })
        return
      }

      console.log('success', success.data);

      if(success.data.success == true && success.data.data.token){

        user = {
          id: success.data.data.id,
          email: success.data.data.email,
          name: success.data.data.name,
          token: success.data.data.token
        }

        console.log('user');
        console.log(user);

        this.props.onLogin(user)

        // Redireciona e reseta a pilha
        this.props
         .navigation
         .dispatch(StackActions.reset({
           index: 0,
           actions: [
             NavigationActions.navigate({ routeName: 'MyLists'})
           ]
          }));

      }
      this.setState({
        textButton: "REDIRECIONANDO"
      })

    }).catch(error => {
      console.log('erro', error)
    })
  }

  _redirectViewTask(idchamado) {

    // this.props.viewtask({
    //   id: idchamado,
    //   usuario_id: this.props.user.id,
    //   token: this.props.user.token
    // });
    //
    // setTimeout( () => {
    //   this.props.mytasks({
    //     id: this.props.user.id,
    //     token: this.props.user.token
    //   });
    //   console.log('updt')
    // },1500)
    //
    // this.setState({
    //   loadPage: 1
    // });
    //
    //
    // this.props.navigation.navigate('ViewTask', {idChamado: idchamado});
  }


  render() {
    const logo = "../assets/imgs/done_icon.png";
    const textLogo = "../assets/imgs/logoText.png";

    const resultss =  SyncStorage.get('user');
    if(resultss){
      // Redireciona e reseta a pilha
      this.props
       .navigation
       .dispatch(StackActions.reset({
         index: 0,
         actions: [
           NavigationActions.navigate({ routeName: 'MyLists'})
         ]
        }));
    }


    return (
      <Container style={ styles.container } >
        <Content>
          <Form>
            <Image  square large source={require( logo)} style={ styles.textLogothumbnail } />
            <Item >
              <Input placeholder="Email" onChangeText={this.manipularUsuario} value={this.state.user } style={ styles.input_login} placeholderTextColor="#9b9b9b" />
            </Item>

            <Item >
              <Input placeholder="Senha" secureTextEntry={true} onChangeText={this.manipularPass} value={this.state.pass } style={ styles.input_login} placeholderTextColor="#9b9b9b" />
            </Item>

            <Button rounded primary style={ styles.buttonLogin } onPress={ () => this._submitLogin() } >
             <Text style={{ alignItems: 'center', justifyContent: 'center', textAlign: 'center',fontSize: 12 }}>{this.state.textButton}</Text>
            </Button>

          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 6,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  input_login: {
    width: '55%',
    fontSize: 18,
    padding: 10,
    paddingRight: 100
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonLogin: {
    margin: 105,
    marginTop: 20,
    marginBottom: 20,
    width: 150,
    flex: 5,
    alignItems: 'center',
  },
  thumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    margin: 135,
    marginBottom: 5,
    marginTop: 50,
  },
  textLogothumbnail: {
    width: 350,
    resizeMode: 'center',
    marginBottom: 1,
    marginTop: 1,
  }
});

const mapStateToProps = ({ user,task,listTask }) => {
  return { user,task,lista_chamados: listTask }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogin: user => dispatch( login(user) )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
