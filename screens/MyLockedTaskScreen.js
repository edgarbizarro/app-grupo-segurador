import React, { Component } from 'react';
import {Platform, StyleSheet, View, Image, Alert, AsyncStorage, FlatList, RefreshControl} from 'react-native';
import { Container, Content, Header, List, ListItem, Thumbnail, Left, Body, Right, Button, Icon, Fab, Title, Text, Badge } from 'native-base';
import {LogoTitle } from '../components/LogoTitle';
import SyncStorage from 'sync-storage';
import Service from '../services/Service';
import { create } from 'apisauce';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ActionButton from 'react-native-action-button';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import { task, viewtask } from '../src/store/actions/task';
import { listTask, teste1, mytasks } from '../src/store/actions/listTask';
import { listlockedTask, lockedListTask } from '../src/store/actions/lockedListTask';
import { notifications } from '../src/store/actions/notifications';



class MyLockedTaskScreen extends Component {

  static navigationOptions = {
      title: 'Encerrados',
      header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      lists: [],
      notificationsTotal: '',
      isRefreshing: false
    };
  }


  componentDidMount(){
    this.props.lockedListTask({
      id: this.props.user.id,
      token: this.props.user.token
    })

    this.props.notifications({
      usuario_id: this.props.user.id,
      token: this.props.user.token
    });

    firebase.notifications().setBadge(this.props.notificacao.notifications)
  }

  createTask(){
    this.props.notifications({
      usuario_id: this.props.user.id,
      token: this.props.user.token
    });

    firebase.notifications().setBadge(this.props.notificacao.notifications)
    this.props.navigation.navigate('NewTask');
  }

  _renderItem = ({item}) => (
    <ListItem key={item.id} thumbnail  onPress={ () => this._redirectViewTask( item.id )}>
      <Left>
        <Thumbnail circle source={{ uri: 'http://erp.ciebe.com.br'+item.usuario_foto }} />
      </Left>
      <Body>
        <Text numberOfLines={2}>{item.titulo}</Text>
        <Text note numberOfLines={1}>{item.atendente_nome}</Text>
        <Text note numberOfLines={1}>{item.prazo}</Text>

        {item.status == 1 &&
          <Badge primary style={ {marginTop: 5} }>
            <Text>Aguardando Atendimento</Text>
          </Badge>
        }
        {item.status == 3 &&
          <Badge success style={ {marginTop: 5} }>
            <Text>Em Atendimento</Text>
          </Badge>
        }
        {item.status == 4 &&
          <Badge info style={ {marginTop: 5} }>
            <Text>Aguardando Aprovação</Text>
          </Badge>
        }
        {item.status == 5 &&
          <Badge danger style={ {marginTop: 5} }>
            <Text>Encerrado</Text>
          </Badge>
        }

      </Body>

    </ListItem>
  );
  
  _keyExtractor = (item, index) => item.id.toString();

  _refresh(){
    this.setState({
      isRefreshing: true
    })

    this.props.lockedListTask({
      id: this.props.user.id,
      token: this.props.user.token
    })

    this.props.notifications({
      usuario_id: this.props.user.id,
      token: this.props.user.token
    });

    firebase.notifications().setBadge(this.props.notificacao.notifications)

  }

  drawerToggle(){
    this.props.navigation.toggleDrawer();
  }

  notificationsToggle(){
    this.props.navigation.navigate('MyNotifications');
  }

  _redirectViewTask(idchamado) {
    this.props.viewtask({
      id: idchamado,
      token: this.props.user.token
    });

    this.props.notifications({
      usuario_id: this.props.user.id,
      token: this.props.user.token
    });

    this.setState({
      loadPage: 1
    });

    this.props.navigation.navigate('ViewTask', {idChamado: idchamado});
  }


  render() {

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' onPress={ () => this.drawerToggle() }/>
            </Button>
          </Left>
          <Body>
            <Text style={ style.styleTitle }>Encerrados</Text>
          </Body>
          <Right badge>
            {this.props.notificacao.notifications > 0 && <Badge ><Text> {this.props.notificacao.notifications}</Text></Badge> }
            <FontAwesome5 name={params.icon_bell.name} color={params.icon_bell.color} onPress={ () => this.notificationsToggle() } size={params.icon_bell.size} />
          </Right>
        </Header>

        <FlatList
            data={this.props.lista_encerrados.chamados}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            refreshControl={
              <RefreshControl
                refreshing={this.props.lista_encerrados.refresh}
                onRefresh={this._refresh.bind(this)}
              />
            }
          />

          <Fab
            key=""
            active="true"
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={ () => this.createTask() }>
            <FontAwesome5 name={'plus'} color={'white'}  size={ 18} />
          </Fab>
      </Container>
    );
  }

}

const mapStateToProps = ({ user,task,listTask, notifications, lockedListTask }) => {
  return { user,task,lista_chamados: listTask, notificacao: notifications, lista_encerrados: lockedListTask }
}

const mapDispatchToProps = {
  task,
  listTask,
  viewtask,
  notifications,
  lockedListTask,
  listlockedTask,
  teste1,
  mytasks,
}

export default connect(mapStateToProps, mapDispatchToProps)(MyLockedTaskScreen)
