import { SOLICITATIONLIST, REMOVE_SOLICITATIONLIST, REFRESH } from './actionTypes'

import Service from '../../../services/Service';

export const listsolicitationTask = task => {
  return {
    type: SOLICITATIONLIST,
    payload: task
  }
}

export const removesolicitationListTask = () => {
  return {
    type: REMOVE_SOLICITATIONLIST
  }
}

export function solicitationListTask(params) {
  return dispatch => {
    dispatch({
      type: REFRESH,
      payload: true
    });
    Service.mysolicitations(params)
      .then( success => {
        dispatch({ type: REFRESH, payload: false });
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: SOLICITATIONLIST,
              payload: success.data.chamados
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }
      }).catch(error => {
        console.log('erro', error)
        dispatch({ type: REFRESH, payload: false });
      })
  }
}
