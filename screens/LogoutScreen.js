/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View, Image, Alert} from 'react-native';
import { Container, Thumbnail, Content, Form, Item, Input, Label, Button, Text, Spinner } from 'native-base';
import { create } from 'apisauce';
import { StackActions, NavigationActions } from 'react-navigation';
import Service from '../services/Service';
import SyncStorage from 'sync-storage';
import { connect } from 'react-redux';
import { login, logout } from '../src/store/actions/user'

class LogoutScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
  }

  state = {
    email: '',
    password: '',
    foto: '',
    token: '',
    nome: '',
    centralizador_id: ''
  }

  componentWillMount(){
    user = {
      id: null,
      name: null,
      email: null,
      super_usuario: null,
      token: null,
      foto: null,
      centralizador_id: null
    }
    this.props.onLogout(user)
  }

  render() {

    return (
      <Container>
        <Content>
            <Text>Saindo...</Text>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ user,task,listTask }) => {
  return { user,task,lista_chamados: listTask }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogin: user => dispatch( login(user) ),
    onLogout: user => dispatch( logout(user))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScreen)
