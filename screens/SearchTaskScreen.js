import React, { Component } from 'react';
import {Platform, StyleSheet, View, Image, Alert, AsyncStorage} from 'react-native';
import { Container, Content, Header, List, ListItem, Thumbnail, Left, Body, Right, Button, Icon, Fab, Title, Text, Badge, Item, Input} from 'native-base';
import {LogoTitle } from '../components/LogoTitle';
import SyncStorage from 'sync-storage';
import Service from '../services/Service';
import { create } from 'apisauce';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ActionButton from 'react-native-action-button';
import { connect } from 'react-redux';
import { task, viewtask } from '../src/store/actions/task';
import { listTask, teste1, mytasks } from '../src/store/actions/listTask';



class SearchTaskScreen extends Component {

  static navigationOptions = {
      title: 'Pesquisar',
      header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      lists: [],
      searchrs: '',
      searchVal: ''
    };

    this.getSearch = this.getSearch.bind(this);
    
  }

  getSearch(searchrs) {
    this.setState({
      searchrs
    });
    this.setState({
      searchVal: searchrs
    })
  }

  drawerToggle(){
    this.props.navigation.toggleDrawer();
  }

  cleanSearch(searchrs = ''){
    console.log('cleanSearch')
    this.getSearch(searchrs)
    // this.setState({
    //   lists: []
    // });
  }

  _getSearch(){
    console.log('Test Value', this.state.searchrs);
    
    data = {
      usuario_id: this.props.user.id,
      superusuario: this.props.user.super_usuario,
      searchrs: this.state.searchrs,
      token: this.props.user.token
    }

    Service.searchtask(data).then( success => {
      this.setState({
        lists: success.data.pesquisas
      })

    }).catch(error => {
      console.log('erro', error)
    })

  }

  notificationsToggle(){
    this.props.navigation.navigate('MyNotifications');
  }

  _redirectViewTask(idchamado) {
    this.props.viewtask({
      id: idchamado,
      token: this.props.user.token
    });

    this.setState({
      loadPage: 1
    });

    this.props.navigation.navigate('ViewTask', {idChamado: idchamado});
  }

  render() {

      return (
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Icon name='menu' onPress={ () => this.drawerToggle() }/>
              </Button>
            </Left>

            <Body>
              <Text style={ style.styleTitle }>Pesquisar</Text>
            </Body>
            <Right>
            </Right>

          </Header>
          <Item >
            <Icon name="ios-search" style={{marginLeft:5}} />
            <Input placeholder="Pesquisar por id, titulo ou descrição..." onChangeText={this.getSearch} value={this.state.searchVal} placeholderTextColor="#9b9b9b" />
            <Right >
              <Button transparent onPress={ () => { this.cleanSearch('') }}>
                <Text style={{ color:'#c9c9c9'}}>Limpar</Text>
              </Button>
            </Right>
          </Item>

          <Button block info onPress={ () => this._getSearch() }>
            <Text>Buscar</Text>
          </Button>
          <Content>


          <List>

            {this.state.lists.map(item => (
              <ListItem key={item.id} thumbnail  onPress={ () => this._redirectViewTask( item.id )}>
                <Left>
                  <Thumbnail circle source={{ uri: 'http://erp.ciebe.com.br'+item.usuario_foto }} />
                </Left>
                <Body>
                  <Text numberOfLines={2}>{item.titulo}</Text>
                  <Text note numberOfLines={1}>Criado: {item.usuario_nome}</Text>

                  {item.status == 1 &&
                    <Badge primary style={ {marginTop: 5} }>
                      <Text>Aguardando Atendimento</Text>
                    </Badge>
                  }
                  {item.status == 3 &&
                    <Badge success style={ {marginTop: 5} }>
                      <Text>Em Atendimento</Text>
                    </Badge>
                  }
                  {item.status == 4 &&
                    <Badge info style={ {marginTop: 5} }>
                      <Text>Aguardando Aprovação</Text>
                    </Badge>
                  }
                  {item.status == 5 &&
                    <Badge danger style={ {marginTop: 5} }>
                      <Text>Encerrado</Text>
                    </Badge>
                  }

                </Body>

              </ListItem>
            ))}

          </List>




          </Content>
        </Container>
      );

  }

}

const mapStateToProps = ({ user,task,listTask }) => {
  return { user,task,lista_chamados: listTask }
}

const mapDispatchToProps = {
  task,
  listTask,
  viewtask,
  teste1,
  mytasks,
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchTaskScreen)
