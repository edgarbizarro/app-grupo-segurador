import React, { Component } from 'react';
import { Container, Header, Content, Thumbnail, Text } from 'native-base';

export default class LogoTitle extends Component {
  render() {
   const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";
   return (
     <Container>
       <Header />
       <Content>
         <Thumbnail square source={{uri: uri}} />
       </Content>
     </Container>
   );
 }
}
