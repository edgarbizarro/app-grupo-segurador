import { SOLICITATIONLIST, REMOVE_SOLICITATIONLIST, REFRESH } from '../actions/actionTypes'

const initialState = {
  chamados: [],
  refresh: false
}

const reducer = (state = initialState, action ) => {
  // console.log('Reducer SOLICITATIONLIST', action);
  switch (action.type) {
    case SOLICITATIONLIST:
      console.log('Reducer SOLICITATIONLIST', action);
      return {
        ...state,
        chamados: action.payload
      }
    case REMOVE_SOLICITATIONLIST:
      return {
        ...state,
        chamados: []
      }
    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      }
    default:
      return state
  }
}

export default reducer
