import {  REMOVE_TASK_ID, TASK } from './actionTypes'
import Service from '../../../services/Service';

export const removeTask = () => {
  return {
    type: REMOVE_TASK_ID
  }
}

export function viewtask(getTask) {
  return dispatch => {
    Service.viewtask(getTask)
      .then( success => {
        if (success.ok) {
          dispatch({
            type: TASK,
            payload: success.data
          });
        } else {
          console.log('Erro na requisição', success.message);
        }
      }).catch(error => {
        console.log('erro', error)
      })
  }
}

// export function addObservation(texto){
//   console.log('Teste');
//   return (dispatch, store) => {
//     console.log('Store enviada pelo Thunk', store().task.task);
//     // if(this.state.description.length != 0){
//     //   Service.addobservation({
//     //     chamado_id: this.props.task.task.chamado.id,
//     //     usuario_id: this.props.user.id,
//     //     texto: texto,
//     //     token: this.props.user.token
//     //   }).then( success => {
//     //     console.log('addobservation success', success)
//     //   }).catch(error => {
//     //     console.log('erro', error)
//     //   })
//     // }else{
//     //   alert('Insira algum texto para observação')
//     // }
//   }
// }
