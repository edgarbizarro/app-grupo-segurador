import { LISTTASK, REMOVE_LISTSTASKS, REFRESH } from '../actions/actionTypes'

const initialState = {
  afiliaetes: [],
  refresh: false
}

const reducer = (state = initialState, action ) => {
  // console.log('Reducer listTask', action);
  switch (action.type) {
    case LISTTASK:
      return {
        ...state,
        afiliaetes: action.payload
      }
    case REMOVE_LISTSTASKS:
      return {
        ...state,
        afiliaetes: []
      }
    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      }
    default:
      return state
  }
}

export default reducer
