import { LOCKEDLIST, REMOVE_LOCKEDLIST, REFRESH } from '../actions/actionTypes'

const initialState = {
  chamados: [],
  refresh: false
}

const reducer = (state = initialState, action ) => {
  switch (action.type) {
    case LOCKEDLIST:
      return {
        ...state,
        chamados: action.payload
      }
    case REMOVE_LOCKEDLIST:
      return {
        ...state,
        chamados: []
      }
    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      }
    default:
      return state
  }
}

export default reducer
