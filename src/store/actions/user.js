import { USER_LOGGED_IN, USER_LOGGED_OUT } from './actionTypes'
import Navigate from 'react-navigation';
import NavigationService from '../../../navigation/Services'

export const login = user => {
  return {
    type: USER_LOGGED_IN,
    payload: user
  }
}

export const logout = () => {
  return dispatch => {
    dispatch({ type: USER_LOGGED_OUT });
    NavigationService.navigate('Login');
  }
}
