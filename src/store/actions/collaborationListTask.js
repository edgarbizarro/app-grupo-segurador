import { COLLABORATIONLIST, REMOVE_COLLABORATIONLIST, REFRESH } from './actionTypes'

import Service from '../../../services/Service';

export const listcollaborationTask = task => {
  return {
    type: COLLABORATIONLIST,
    payload: task
  }
}

export const removecollaborationListTask = () => {
  return {
    type: REMOVE_COLLABORATIONLIST
  }
}

export function collaborationListTask(params) {
  return dispatch => {
    dispatch({
      type: REFRESH,
      payload: true
    });
    Service.myrequests(params)
      .then( success => {
        dispatch({ type: REFRESH, payload: false });
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: COLLABORATIONLIST,
              payload: success.data
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }
      }).catch(error => {
        console.log('erro', error)
        dispatch({ type: REFRESH, payload: false });
      })
  }
}


