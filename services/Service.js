
import {Platform, StyleSheet, View, Image, Alert} from 'react-native';
import { create } from 'apisauce'
import { connect } from 'react-redux';
import { persis } from '../src/store/storeConfig';
import { logout } from '../src/store/actions/user'

import NavigationService from '../navigation/Services'


localhost = 0

if(localhost == 1){
  baseUrl = "http://localhost/afiliados/api"
}else{
  baseUrl = "https://afiliado.gruposegurador.com.br/api"
}

const api = create({
  baseURL: baseUrl,
  headers:{
    Accept: 'application/json'
  }
});

const naviMonitor = response => {
  if(response.status == 500 || response.status == 400 || response.status == 401){
    // if(response.message == "Expired token"){
      console.log("Unathorized")
      user = {
        id: null,
        name: null,
        email: null,
        token: null
      }
      persis.dispatch( logout(user) )
    // }
  }
}


api.addMonitor(naviMonitor)

const login = data => {
  return api.post('/usuarios/login', data)
}


const mylists = data => {
  return api.post('/usuarios/list_afiliaetes', {},{ headers: { 'Authorization': 'Bearer ' + data.token } })
}

const mylistspending = data => {
  return api.post('/usuarios/list_afiliaetes_pending', {},{ headers: { 'Authorization': 'Bearer ' + data.token } })
}

const viewtask = data => {
  return api.post('/usuarios/view_afiliaete', { id: data.id },{ headers: { 'Authorization': 'Bearer ' + data.token, 'charset':'utf-8'} })
}

const createtask = data => {
  return api.post('/usuarios/new-afiliaete', {
    name: data.name,
    email: data.email,
    phone: data.phone,
    company: data.company,
    birthday: data.birthday,
    city: data.city,
    state: data.state,
    observation: data.observation,
  },{ headers: { 'Authorization': 'Bearer ' + data.token, 'charset':'utf-8'} })
}

export default {
  api,
  login,
  createtask,
  viewtask,
  mylists,
  mylistspending
}
