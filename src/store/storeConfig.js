import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import userReducer from './reducers/user'
import taskReducer from './reducers/task'
import listTaskReducer from './reducers/listTask'
import notificationsReducer from './reducers/notifications'
import lockedListTaskReducer from './reducers/lockedListTask'
import solicitationListTaskReducer from './reducers/solicitationListTask'
import collaborationListTaskReducer from './reducers/collaborationListTask'
import notificationListTaskReducer from './reducers/notificationListTask'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
}

const reducers = combineReducers({
  user: userReducer,
  task: taskReducer,
  listTask: listTaskReducer,
  lockedListTask: lockedListTaskReducer,
  solicitationListTask: solicitationListTaskReducer,
  collaborationListTask: collaborationListTaskReducer,
  notificationListTask: notificationListTaskReducer,
  notifications: notificationsReducer
})

const persistedReducer = persistReducer(persistConfig, reducers)
const persis = createStore(persistedReducer, applyMiddleware(thunk))
const persistor = persistStore(persis)

// const storeConfig = () => {
//   return createStore(reducers, applyMiddleware(thunk))
// }

export {persis, persistor};
