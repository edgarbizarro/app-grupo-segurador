import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';

const style = StyleSheet.create({

        styleTitle: {
            color: Platform.OS == "android" ? "white" : 'black',
            fontSize: Platform.OS == "android" ? 20 : 17,
            flexWrap: 'nowrap'
        },
        Fab_style:{
            backgroundColor: '#5067FF'
        },

        cardContact: {
          backgroundColor: '#a0a0a0',
          borderRadius: 5
        },

        // AttachmentTaskScreen
        AttachmentTaskScreen_emptyAttachment:{
            alignItems:'center',
            textAlign:'center',
            marginTop:'50%'
        },
        AttachmentTaskScreen_listComponent:{
            marginBottom: 100
        },
        AttachmentTaskScreen_SwipeRow_View:{
            paddingLeft: 10
        },
        AttachmentTaskScreen_SwipeRow_Text:{
            textAlign: 'left',
            alignSelf: 'stretch'
        },
        AttachmentTaskScreen_ModalImage_View:{
            marginTop: 22
        },
        AttachmentTaskScreen_ModalImage_Image:{
            height: '70%',
            alignContent: 'center',
            justifyContent:'center',
            marginTop: 100
        },
        AttachmentTaskScreen_ModalImage_Footer:{
            backgroundColor: 'white',
            position:'absolute',
            bottom: -70
        },
        AttachmentTaskScreen_ModalImage_Button:{
            margin: 6,
            padding: 20,
            borderRadius: 30
        },
        AttachmentTaskScreen_ModalRename_View:{
            flex:1,
            alignItems: 'center',
            justifyContent: 'center',
            height: 600
        },
        AttachmentTaskScreen_ModalRename_Item:{
            width: 380,
            marginTop: 260
        },
        AttachmentTaskScreen_ModalRename_Text:{
            fontSize: 10
        },
        AttachmentTaskScreen_ModalRename_Row:{
            marginLeft: 5
        },
        AttachmentTaskScreen_ModalRename_Button:{
            margin: 6,
            padding: 20,
            borderRadius: 30,
            textAlign:'center'
        },
        AttachmentTaskScreen_Fab_Color:{
            backgroundColor: '#5067FF'
        },
        AttachmentTaskScreen_Fab_DocumentButton:{
            backgroundColor: '#529FF3'
        },
        AttachmentTaskScreen_Fab_GalleryButton:{
            backgroundColor: '#34A34F'
        },
        AttachmentTaskScreen_Fab_CameraButton:{
            backgroundColor: '#3B5998'
        },


        // NewTasksScreen
        NewTasksScreen_Form:{
            margin: 8
        },
        NewTasksScreen_Title:{
            marginTop: 10
        },
        NewTasksScreen_Picker_textStyle:{
            color: "#5cb85c"
        },
        NewTasksScreen_Picker_itemStyle:{
            backgroundColor: "#d3d3d3",
            marginLeft: 0,
            paddingLeft: 10
        },
        NewTasksScreen_Picker_itemTextStyle:{
            color: '#788ad2'
        },
        NewTasksScreen_LabelDescription:{
            marginTop: 5
        },
        NewTasksScreen_View:{
            borderBottomColor: '#d3d3d3',
            borderBottomWidth: 1
        },
        NewTasksScreen_LabelAttachment:{
            marginTop: '5%',
            alignSelf: 'center',
            alignItems: 'center'
        },
        NewTasksScreen_Content:{
            marginBottom: 55
        },
        NewTaskScreen_TextEmpty:{
            color: 'grey',
            marginTop: 10,
            marginBottom: 10
        },
        NewTaskScreen_FileButton:{
            margin: 6,
            padding: 20,
            borderRadius: 30,
        },
        NewTaskScreen_RenameButton:{
            margin: 6,
            padding: 20,
            borderRadius: 30
        },
        NewTaskScreen_RemoveButton:{
            margin: 6,
            padding: 20,
            borderRadius: 30
        },
        NewTaskScreen_ModalImageView:{
            height: '70%',
            alignContent: 'center',
            justifyContent:'center',
            marginTop: 100
        },
        NewTaskScreen_ModalImageButton:{
            margin: 6,
            padding: 20,
            borderRadius: 30
        },
        NewTaskScreen_ModalImageFooter:{
            backgroundColor: 'white',
            position:'absolute',
            bottom: -70
        },
        NewTaskScreen_LabelDescription:{
            marginBottom: 5
        },
        NewTaskScreen_ModalImageButtonFooter:{
            margin: 6,
            padding: 20,
            borderRadius: 30,
            textAlign:'center'
        },

        // AddContributor
        AddContributor_Fab_style:{
            backgroundColor: '#5067FF'
        },
        AddContributor_Fab_ButtonWhatsApp:{
            backgroundColor: '#34A34F'
        },
        AddContributor_Fab_ButtonFacebook:{
            backgroundColor: '#3B5998'
        },
        AddContributor_Fab_ButtonMail:{
            backgroundColor: '#DD5144'
        },

        // MyTasksScreen
        MyTasksScreen_Spinner:{
            marginTop: '50%',
            justifyContent: 'center',
            alignItems: 'center'
        },
        MyTasksScreen_Badge:{
            marginTop: 5
        },
        MyTasksScreen_Fab:{
            backgroundColor: '#5067FF'
        },


  });

  export default style
