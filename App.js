import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator ,
} from 'react-navigation';
import { View, Button, Text} from 'react-native';
import React from 'react';
// import React from "react";
import LoginScreen from './screens/LoginScreen';
import MyListsScreen from './screens/MyListsScreen';
import MyListsPendingScreen from './screens/MyListsPendingScreen';
import NewTaskScreen from './screens/NewTaskScreen';
import ViewTaskScreen from './screens/ViewTaskScreen';
import LogoutScreen from './screens/LogoutScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


class LogoutButton extends React.Component{

  render(){
    return (
      <View>
        <Button><Text><FontAwesome5 name={'power-off'}  /> Sair</Text></Button>
      </View>
    )
  }

}

const ViewNavigation = createStackNavigator({
  ViewTask: {
    screen: ViewTaskScreen
  }
});

const DrawerStack = createDrawerNavigator({

  NewTask: {
    screen: NewTaskScreen,
  },

  MyLists: {
    screen: MyListsScreen,
  },

  MyListsPending: {
    screen: MyListsPendingScreen,
  },

  ViewTask: {
    screen: ViewNavigation,
    navigationOptions: {
        drawerLabel: (): any => null,
    }
  },

  Logout: {
    screen: LogoutScreen,
    navigationOptions: {
      title: 'Sair',
    }
  },

},{
  initialRouteName: 'MyLists',
  // contentComponent: () => (
  //   <View>
  //     <Text onPress={ this.toNotifications() }>Hello</Text>
  //   </View>
  // )
});

const AppNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen
  },
  MyLists: {
    screen: DrawerStack
  },
}, { headerMode: 'none' } );

export default createAppContainer(AppNavigator);
