import { NOTIFICATIONS, CLEAR_NOTIFICATIONS } from '../actions/actionTypes'

const initialState = {
  notifications: null
}

const reducer = (state = initialState, action ) => {
    switch (action.type) {
      case NOTIFICATIONS:
      return {
        ...state,
        notifications: action.payload
      }
    case CLEAR_NOTIFICATIONS:
      return {
        ...state,
        notifications: null
      }
    default:
      return state
  }
}

export default reducer
