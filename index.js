/**
 * @format
 */
import React from 'react';
import { Provider } from 'react-redux';
import {AppRegistry, ActivityIndicator} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { persis, persistor}  from './src/store/storeConfig';
import { PersistGate } from 'redux-persist/integration/react';
import NavigationService from './navigation/Services';
import style from './src/style'
import params from './services/params'

window.style = style;
window.params = params;

renderLoading = () => (
  <View>
    <ActivityIndicator size="large" />
  </View>
)

const Redux = () =>(
  <Provider store={persis}>
    <PersistGate persistor={persistor} loading={null}>
      <App ref={ stack => NavigationService.setTopLevelNavigator(stack) } />
    </PersistGate>
  </Provider>
)

AppRegistry.registerComponent(appName, () =>  Redux );
