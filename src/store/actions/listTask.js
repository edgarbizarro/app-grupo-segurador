import { LISTTASK, REMOVE_LISTTASK, REFRESH } from './actionTypes'

import Service from '../../../services/Service';

export const listTask = task => {
  return {
    type: LISTTASK,
    payload: task
  }
}

export const removeListTask = () => {
  return {
    type: REMOVE_LISTTASK
  }
}

export function mylists(login) {
  return dispatch => {

    dispatch({
      type: REFRESH,
      payload: true
    });
    Service.mylists(login)
      .then( success => {
        dispatch({ type: REFRESH, payload: false });
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: LISTTASK,
              payload: success.data.data
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }

      }).catch(error => {
        console.log('erro', error)
        dispatch({ type: REFRESH, payload: false });
      })
  }
}


export function listcontributors(login) {
  return dispatch => {
    Service.listcontributors(login)
      .then( success => {
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: LISTTASK,
              payload: success.data
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }
      }).catch(error => {
        console.log('erro listTask', error)
      })
  }
}
