import { NOTIFICATIONLIST, REMOVE_NOTIFICATIONLIST, REFRESH } from './actionTypes'

import Service from '../../../services/Service';

export const listnotificationTask = task => {
  return {
    type: NOTIFICATIONLIST,
    payload: task
  }
}

export const removenotificationListTask = () => {
  return {
    type: REMOVE_NOTIFICATIONLIST
  }
}

export function notificationListTask(params) {
  return dispatch => {
    dispatch({
      type: REFRESH,
      payload: true
    });
    Service.mynotifications(params)
      .then( success => {
        dispatch({ type: REFRESH, payload: false });
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: NOTIFICATIONLIST,
              payload: success.data.chamados
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }
      }).catch(error => {
        console.log('erro', error)
        dispatch({ type: REFRESH, payload: false });
      })
  }
}


