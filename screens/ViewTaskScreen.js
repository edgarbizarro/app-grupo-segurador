import React, { Component } from 'react';
import {Platform, StyleSheet, View, Image, Alert, AsyncStorage, Modal, TouchableOpacity, ActionSheetIOS} from 'react-native';
import { Container, Content, Header,
         List, ListItem, Thumbnail,
         Left, Body, Right,
         Button, Icon, Fab,
         Title, Text, Badge,
         Footer, FooterTab, Form,
         Input, Item, Textarea,
         Label, Spinner, Dimensions,
         DatePicker, TouchableHighlight,
         Toast, Row, Card, CardItem } from 'native-base';
import {LogoTitle } from '../components/LogoTitle';
import SyncStorage from 'sync-storage';
import Service from '../services/Service';
import { create } from 'apisauce';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ActionSheet from 'react-native-actionsheet';
import ActionButton from 'react-native-action-button';
import { connect } from  'react-redux';
import { notifications } from '../src/store/actions/notifications';
import { taskId, viewtask } from '../src/store/actions/task';
import { listTask, mytasks, teste2 } from '../src/store/actions/listTask';
import moment from "moment";
import style from '../src/style'
// import params from '../services/params';

class ViewTaskScreen extends Component {

  static navigationOptions = {
      title: 'Visualizar',
      header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      afiliaete: [],
      loadPage: 0,
      modalFinalized: false,
      modalFinalizedVisible: false,
      textFinalized: '',
      modalReproved: false,
      modalReprovedVisible: false,
      textReproved: ''
    }
  }



  drawerToggle(){
    this.props.navigation.toggleDrawer();
  }






  componentDidUpdate(){
    if(this.state.loadPage != 1){
      console.log(this.props.task.task.data.contacts)

      this.setState({
        afiliaete: this.props.task.task.data
      })

      this.setState({ loadPage: 1 })
    }
  }

  render() {
    if(this.state.loadPage == 0) {
      return <Spinner color='blue' style={{ marginTop: '50%', justifyContent: 'center', alignItems: 'center' }} />
    }else{
      return (
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Icon name='menu' onPress={ () => this.drawerToggle() }/>
              </Button>
            </Left>
            <Body>
              <Text style={ style.styleTitle }>Visualizar</Text>
            </Body>
          </Header>
          <Content padder style={{ marginRight: 15}}>

            <Form>
              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Nome</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.name}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Email</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.email}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Telefone</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.phone}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Data nascimento</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.birthday}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Empresa</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.company}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Cidade</Label>
                <Text style={{ marginTop: 10, alignSelf: 'flex-start' }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.city} - {this.state.afiliaete.afiliaete.state}</Text>
              </Item>

              <Item stackedLabel>
                <Label style={{marginBottom: 8}}>Observação</Label>
                <Text style={{ marginTop: 10 }} adjustsFontSizeToFitWidth >{this.state.afiliaete.afiliaete.observation} - {this.state.afiliaete.afiliaete.state}</Text>
              </Item>

              <View style={styles.line} />

                { this.state.afiliaete.contacts.map( (data, k) => (
                   <Card key={k} style={{backgroundColor: '#a0a0a0', borderRadius: 5, marginLeft: 15}}>
                       <CardItem>
                         <Body>
                           <Text>
                             {data.text}
                           </Text>
                           <View style={styles.line} />
                           <Text style={{ fontSize: 10, color: '#a5a5a5'}}>
                              { moment(data.created).format('D/MM/YYYY') }
                           </Text>
                         </Body>
                       </CardItem>
                     </Card>
               ))}



            </Form>
          </Content>

        </Container>

      );
    }

  }

}


const styles =  StyleSheet.create({
  line: { borderBottomColor: '#d3d3d3', borderBottomWidth: 1, marginTop: 8, marginBottom: 8, marginLeft: 15}
})

export default connect(
  estado => ({
    task: estado.task,
    user: estado.user,
    notificacao: estado.notifications
  }),{
    listTask,
    viewtask,
    notifications,
    teste2,
    mytasks
  }
)(ViewTaskScreen);
