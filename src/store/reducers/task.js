import { REMOVE_TASK_ID, TASK } from '../actions/actionTypes'

const initialState = {
  task: {
    anexos: [],
    chamado: {},
    historico: [],
    observacao: [],
    visualizadores: [],
  }
}

const reducer = (state = initialState, action ) => {
  switch (action.type) {
    case TASK:
      return {
        ...state,
        task: action.payload
      }
    case REMOVE_TASK_ID:
      return {
        ...state,
        task: {}
      }
    default:
      return state
  }
}

export default reducer