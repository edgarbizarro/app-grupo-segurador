import { LOCKEDLIST, REMOVE_LOCKEDLIST, REFRESH } from './actionTypes'

import Service from '../../../services/Service';

export const listlockedTask = task => {
  return {
    type: LOCKEDLIST,
    payload: task
  }
}

export const removelockedListTask = () => {
  return {
    type: REMOVE_LOCKEDLIST
  }
}

export function lockedListTask(params) {
  return dispatch => {
    dispatch({
      type: REFRESH,
      payload: true
    });
    Service.mylocked(params)
      .then( success => {
        dispatch({ type: REFRESH, payload: false });
        if (success.ok) {
          setTimeout(() => {
            dispatch({
              type: LOCKEDLIST,
              payload: success.data.chamado
            });
          }, 2000);
        } else {
          console.log('Erro na requisição', success);
        }
      }).catch(error => {
        console.log('erro', error)
        dispatch({ type: REFRESH, payload: false });
      })
  }
}


